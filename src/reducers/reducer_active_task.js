import { TASK_SELECTED } from '../actions/index';

export default (state = null, action) => {
    switch (action.type) {
        case TASK_SELECTED:
            return action.payload;
    }
    return state;
}