import { GET_TEST } from '../actions/index';

export default (state = null, action) => {
    switch (action.type) {
        case GET_TEST:
            return action.payload;
    }
    return state;
}