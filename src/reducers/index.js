import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import loginUserReducer from './reducer_login_user';
import getTestReducer from './reducer_get_test';
import activeTaskReducer from './reducer_active_task';

const rootReducer = combineReducers({
  form: formReducer,
  userLogged: loginUserReducer,
  test: getTestReducer,
  activeTask: activeTaskReducer
});

export default rootReducer;