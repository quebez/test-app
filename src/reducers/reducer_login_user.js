import { LOGIN_USER } from '../actions/index';

export default (state = { logged: false }, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return action.payload;
    }
    return state;
}