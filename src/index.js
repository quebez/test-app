import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import promiseMiddleware from 'redux-promise';

import reducers from './reducers';

import LoginIndex from './containers/login_index';
import TestIndex from './containers/test_index';
import AdminIndex from './containers/admin_index';


const createStoreWithMiddleware = applyMiddleware(promiseMiddleware)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <Switch>
        <Route path="/test" component={TestIndex} />
        <Route path="/admin" component={AdminIndex} />
        <Route path="/" component={LoginIndex} />
      </Switch>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));