import { promiseMock, getMockTest } from './mock/mock';


export const LOGIN_USER = 'LOGIN_USER';
export const TASK_SELECTED = 'TASK_SELECTED';
export const GET_TEST = 'GET_TEST';

export function loginUser(values) {
    //TODO: get from API users
    const request = promiseMock(values);

    return {
        type: LOGIN_USER,
        payload: request
    };
}

export function getTest(testName) {
    //TODO: get from API testName
    const test = getMockTest(testName);

    return {
        type: GET_TEST,
        payload: test
    }
}

export function selectTask(task) {
    return {
        type: TASK_SELECTED,
        payload: task
    };
}