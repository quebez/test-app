//mock users
const users = {
    'admin@gmail.com': { password: 'kiklop', userType: 'admin' },
    'candidate@gmail.com': { password: 'kiklop', userType: 'candidate', test: 'example' },
    'candidateNoTest@gmail.com': { password: 'kiklop', userType: 'candidate', test: 'thisTestIsNotReal' }
};

//mock promise
export const promiseMock = (values) => {
    return new Promise((resolve, reject) => {
        process.nextTick(() => {
            const email = users[values.email];
            if (!email) {
                reject({ logged: false, error: 'user doesn\'t exist' });
            } else {
                const { password, userType, test } = email;
                password == values.password
                    ? resolve({ logged: true, userType, test })
                    : reject({ logged: false, error: 'password doesn\'t match' });
            }
        });
    });
}

//mock test
export const getMockTest = (testName) => {
    if (testName === 'example') {
        return {
            "testName": "example",
            "testDescription": "This test is just an example.",
            "testAuthor": "Martin Janik",
            "testVersion": "1.0",
            "tasks": [
                {
                    "id": 1,
                    "header": "About yourself",
                    "question": "Tell us something about yourself.",
                    "responseType": "text"
                },
                {
                    "id": 2,
                    "header": "Warm up",
                    "question": "Write us simple hello world in anly programming language.",
                    "responseType": "textarea"
                },
                {
                    "id": 3,
                    "header": "Catch them all",
                    "question": "Catch all the pokemons.",
                    "responseType": "checkbox",
                    "options": ["Coffeescript", "Ecmascript", "Charlizard"]
                }
            ]
        }
    } else {
        return {
            "error": "test not found"
        }
    }
}