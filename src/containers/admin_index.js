import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class AdminIndex extends Component {
    componentWillMount() {
        const { logged, userType } = this.props.userLogged;

        if (!logged || userType !== 'admin') {
            this.props.history.push('/');
        }
    }

    render() {
        return (
            <div>
                Admin Index
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { userLogged: state.userLogged }
}

export default connect(mapStateToProps)(AdminIndex);