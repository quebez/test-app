import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectTask, getTest } from '../actions';
import TestTask from './test_task';

class TestIndex extends Component {
    componentWillMount() {
        const { logged, userType, test } = this.props.userLogged;

        if (!logged || userType !== 'candidate') {
            this.props.history.push('/');
        }

        if (!this.props.test && test) {
            this.props.getTest(test);
        }
    }

    renderTasksMenu() {
        return this.props.test.tasks.map(task => {
            return (
                <li
                    key={task.id}
                    className="list-group-item"
                    onClick={() => this.props.selectTask(task)}
                >
                    {task.header}
                </li>
            )
        });
    }

    render() {
        if (!this.props.test) {
            return (
                <div>
                    Loading...
                </div>
            );
        } else if (this.props.test.error) {
            return (
                <div>
                    Ooops, somthing happened: {this.props.test.error}
                </div>
            );
        }

        const {testName, testDescription, testAuthor} = this.props.test;

        return (
            <div className="navigation">
                Test config: {testName}<br></br>
                Test author: {testDescription}<br></br>
                Test author: {testAuthor}<br></br>
                <br></br>
                <ul className="list-group col-sm-4">
                    {this.renderTasksMenu()}
                </ul>
                <TestTask />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { 
        userLogged: state.userLogged,
        test: state.test
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ 
        selectTask: selectTask,
        getTest: getTest
     }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TestIndex);