import React, { Component } from 'react';
import { connect } from 'react-redux';

class TestTask extends Component {
    render() {
        if (!this.props.task) {
            return <div></div>;
        }

        const { header, question, responseType, options } = this.props.task;

        return (
            <div>
                Test Task<br></br>
                {header}<br></br>
                {question}<br></br>
                <input type={responseType} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        task: state.activeTask
    };
}

export default connect(mapStateToProps)(TestTask);