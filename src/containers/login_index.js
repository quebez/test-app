import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { loginUser } from '../actions';

class LoginIndex extends Component {
    componentWillReceiveProps(nextProps) {
        const { logged, userType } = nextProps.userLogged;
        const { history } = this.props;

        if (logged && userType === 'admin') {
            history.push('/admin')
        } else if (logged && userType === 'candidate') {
            history.push('/test')
        }
    }

    renderField(field) {
        const { meta: { touched, error } } = field;
        const mainClass = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={mainClass}>
                <label>{field.label}</label>
                <input
                    type={field.type}
                    className="form-control"
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ""}
                </div>
            </div>
        );
    }

    onSubmit(values) {
        this.props.loginUser(values);
    }

    render() {
        const { handleSubmit } = this.props;

        return (
            <form
                className="login-form"
                onSubmit={handleSubmit(this.onSubmit.bind(this))}
            >
                <Field
                    name="email"
                    label="Email:"
                    type="text"
                    component={this.renderField}
                />
                <Field
                    name="password"
                    label="Password:"
                    type="password"
                    component={this.renderField}
                />
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
                <div className="text-help">
                    { this.props.userLogged.error }
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};
    const { email, password } = values;
    const REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!email) {
        errors.email = "Enter email.";
    } else if (!REGEX_EMAIL.test(email)) {
        errors.email = "Enter valid email format.";
    }

    if (!password) {
        errors.password = "Enter password.";
    }

    return errors;
}

function mapStateToProps(state) {
    return { userLogged: state.userLogged }
}

export default reduxForm({
    form: 'LoginIndex',
    validate
})(
    connect(mapStateToProps, { loginUser })(LoginIndex)
);