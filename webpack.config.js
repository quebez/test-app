module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      { //not used
        test: /\.json$/,
        loader: 'json-loader',
        include: /config/
      },
      {
        exclude: [/node_modules/, /config/],
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-1']
        }
      }
    ]
  },

  devServer: {
    historyApiFallback: true,
    contentBase: './'
  }
};
